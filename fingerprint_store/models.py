from django.db import models


class Plugin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    name = models.CharField(max_length=50, null=True)
    filename = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=100, null=True)
    version = models.CharField(max_length=10, null=True)

    def __eq__(self, other):
        if self.name == other.name \
                and self.filename == other.filename \
                and self.description == other.description \
                and self.version == other.version:
            return True
        else:
            return False


class Device(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    brand = models.CharField(max_length=20, null=True)
    family = models.CharField(max_length=20, null=True)
    model = models.CharField(max_length=20, null=True)

    def __eq__(self, other):
        if self.brand == other.brand \
                and self.family == other.family \
                and self.model == other.model:
            return True
        else:
            return False


class OperatingSystem(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    family = models.CharField(max_length=20, null=True)
    major = models.CharField(max_length=5, null=True)
    minor = models.CharField(max_length=5, null=True)
    patch = models.CharField(max_length=5, null=True)
    patch_minor = models.CharField(max_length=5, null=True)

    def __eq__(self, other):
        if self.brand == other.brand \
                and self.family == other.family \
                and self.major == other.major \
                and self.minor == other.minor\
                and self.patch_minor == other.patch_minor\
                and self.patch == other.patch:
            return True
        else:
            return False


class UserAgent(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    device = models.ForeignKey(
        Device, blank=True, null=True, on_delete=models.SET_NULL)
    os = models.ForeignKey(
        OperatingSystem, blank=True, null=True, on_delete=models.SET_NULL)

    full_name = models.CharField(max_length=150)
    family = models.CharField(max_length=20, null=True)
    major = models.CharField(max_length=5, null=True)
    minor = models.CharField(max_length=5, null=True)
    patch = models.CharField(max_length=5, null=True)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class Fingerprint(models.Model):
    # Attributes
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    language = models.CharField(max_length=5)
    languages = models.CharField(max_length=30)

    doNotTrack = models.BooleanField(null=True)
    cookiesEnabled = models.BooleanField(null=True)

    timezone = models.IntegerField()

    resolution = models.CharField(max_length=15)

    plugins = models.ManyToManyField(Plugin)
    userAgent = models.ForeignKey(
        UserAgent, blank=True, null=True, on_delete=models.SET_NULL)

    def __hash__(self):
        return hash((self.language, self.languages, self.doNotTrack, self.cookiesEnabled,
                     self.timezone, self.resolution, self.userAgent.full_name,
                     self.plugins))

    def __eq__(self, other):
        s1 = set([','.join([p.name, p.filename, p.description, p.version])
                 for p in self.plugins.all()])
        s2 = set([','.join([p.name, p.filename, p.description, p.version])
                 for p in other.plugins.all()])

        if self.language == other.language \
                and self.languages == other.languages \
                and self.doNotTrack == other.doNotTrack\
                and self.cookiesEnabled == other.cookiesEnabled \
                and self.userAgent.full_name == other.userAgent.full_name\
                and self.resolution == other.resolution \
                and self.timezone == other.timezone \
                and s1 == s2:
            return True
        else:
            return False
