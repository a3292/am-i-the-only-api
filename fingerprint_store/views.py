from fingerprint_store.models import Fingerprint
from fingerprint_store.utils import createFingerprintArray, createLanguagesString, createPulginEntry, createUserAgentEntry
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def create_fingerprint(request):
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body)

            # RETRIEVE DATA
            language = json_data.get("language", None)
            languages = json_data.get("languages", None)
            doNotTrack = json_data.get("doNotTrack", False)
            cookiesEnabled = json_data.get("cookiesEnabled", False)
            timezone = json_data.get("timezone", 0)
            resolution = json_data.get("resolution", "")
            userAgent = json_data.get("userAgent", None)
            plugins = json_data.get("plugins", None)
            id = json_data.get("id", None)

            ua = createUserAgentEntry(userAgent)
            ua.save()
            ps = [createPulginEntry(plugin) for plugin in plugins]

            try:
                fingerprint = Fingerprint.objects.get(id=id)
                fingerprint.language = language
                fingerprint.languages = createLanguagesString(languages)
                fingerprint.doNotTrack = doNotTrack
                fingerprint.cookiesEnabled = cookiesEnabled
                fingerprint.resolution = resolution
                fingerprint.timezone = timezone
                fingerprint.userAgent = ua
                fingerprint.plugins.clear()
            except:
                fingerprint, created = Fingerprint.objects.get_or_create(
                    language=language,
                    languages=createLanguagesString(languages),
                    doNotTrack=doNotTrack,
                    cookiesEnabled=cookiesEnabled,
                    resolution=resolution,
                    timezone=timezone,
                    userAgent=ua
                )

            for p in ps:
                p.save()
                fingerprint.plugins.add(p)

            fingerprint.save()
            response = {
                "hash": hash(fingerprint),
                "id": fingerprint.id,
                "message": "Ok !!",
                "error": False
            }
            return JsonResponse(response, safe=False)
        except:
            response = {
                "message": "Error occured",
                "error": True
            }
            return JsonResponse(response, safe=False)
    else:
        response = {
            "message": "Method Not Authorized",
            "error": True
        }
        return JsonResponse(response, safe=False)


def get_fingerprint(request):
    if request.method == 'GET':
        try:
            id = int(request.GET.get('id', 0))
            fp = Fingerprint.objects.get(id=id)
            fps = Fingerprint.objects.all()
            total = len(fps)
            count = sum([fp == sfp for sfp in fps])
            if count > 1:
                same = True
            else:
                same = False
            fingerprint = createFingerprintArray(fp, total)

            response = {
                "id": id,
                "same": same,
                "count": count,
                "total": total,
                "hash": hash(fp),
                "fingerprint": fingerprint,
                "message": "OK !!!",
                "error": False
            }
            return JsonResponse(response, safe=False)
        except:
            response = {
                "message": "Error occured",
                "error": True
            }
            return JsonResponse(response, safe=False)
    else:
        response = {
            "message": "Method Not Authorized",
            "error": True
        }
        return JsonResponse(response, safe=False)
