from django.apps import AppConfig


class FingerprintStoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fingerprint_store'
