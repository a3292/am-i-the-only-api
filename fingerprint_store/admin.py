from django.contrib import admin

from fingerprint_store.models import Plugin, Device, OperatingSystem, UserAgent, Fingerprint

admin.site.register(Plugin)
admin.site.register(Device)
admin.site.register(OperatingSystem)
admin.site.register(UserAgent)
admin.site.register(Fingerprint)
