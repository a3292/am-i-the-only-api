from ua_parser import user_agent_parser
from fingerprint_store.models import Plugin, Device, OperatingSystem, UserAgent, Fingerprint


def createUserAgentEntry(userAgent):
    parsed_string = user_agent_parser.Parse(userAgent)
    # create os
    os, created = OperatingSystem.objects.get_or_create(
        family=parsed_string["os"]["family"],
        major=parsed_string["os"]["major"],
        minor=parsed_string["os"]["minor"],
        patch=parsed_string["os"]["patch"],
        patch_minor=parsed_string["os"]["patch_minor"],
    )
    # create device
    device, created = Device.objects.get_or_create(
        family=parsed_string["device"]["family"],
        brand=parsed_string["device"]["brand"],
        model=parsed_string["device"]["model"],
    )

    # create useragent
    ua = UserAgent.objects.create(
        device=device,
        os=os,
        full_name=userAgent,
        family=parsed_string["user_agent"]["family"],
        major=parsed_string["user_agent"]["major"],
        minor=parsed_string["user_agent"]["minor"],
        patch=parsed_string["user_agent"]["patch"],
    )

    return ua


def createPulginEntry(plugin):
    plugin, created = Plugin.objects.get_or_create(
        name=plugin["name"],
        filename=plugin["filename"],
        description=plugin["description"],
        version=plugin["version"],
    )

    return plugin


def createLanguagesString(languages):
    return ','.join(languages)


def createPluginsSet(plugins):
    return set([','.join([p.name, p.filename, p.description, p.version])
                for p in plugins.all()])


def createFingerprintArray(fp, total):
    ua = {
        "codename": "USER_AGENT",
        "value": fp.userAgent.full_name,
        "ratio": len(Fingerprint.objects.filter(userAgent__full_name=fp.userAgent.full_name)) / total*100
    }

    navigator = {
        "codename": "USER_AGENT",
        "value": fp.userAgent.family,
        "ratio": len(Fingerprint.objects.filter(userAgent__family=fp.userAgent.family)) / total*100
    }

    device = {
        "codename": "DEVICE",
        "value": fp.userAgent.device.family,
        "ratio": len(Fingerprint.objects.filter(userAgent__device=fp.userAgent.device)) / total*100
    }

    os = {
        "codename": "OPERATING_SYSTEM",
        "value": fp.userAgent.os.family,
        "ratio": len(Fingerprint.objects.filter(userAgent__os=fp.userAgent.os)) / total*100
    }

    lang = {
        "codename": "LANGUAGE",
        "value": fp.language,
        "ratio": len(Fingerprint.objects.filter(language=fp.language)) / total*100
    }

    langs = {
        "codename": "LANGUAGES",
        "value": fp.languages.split(','),
        "ratio": len(Fingerprint.objects.filter(languages=fp.languages)) / total*100
    }

    timezone = {
        "codename": "TIME_ZONE",
        "value": fp.timezone,
        "ratio": len(Fingerprint.objects.filter(timezone=fp.timezone)) / total*100
    }

    resolution = {
        "codename": "RESOLUTION",
        "value": fp.resolution,
        "ratio": len(Fingerprint.objects.filter(resolution=fp.resolution)) / total*100
    }

    ce = {
        "codename": "COOKIES_ENABLED",
        "value": fp.cookiesEnabled,
        "ratio": len(Fingerprint.objects.filter(cookiesEnabled=fp.cookiesEnabled)) / total*100
    }

    dnt = {
        "codename": "DO_NOT_TRACK",
        "value": fp.doNotTrack,
        "ratio": len(Fingerprint.objects.filter(doNotTrack=fp.doNotTrack)) / total*100
    }

    ps = {
        "codename": "PLUGINS",
        "value": [p.name for p in fp.plugins.all()],
        "ratio": getSamePluginsRatio(fp)
    }
    return [ua, device, os, lang, langs, timezone, resolution, ce, dnt, ps]


def getSamePluginsRatio(fp):
    fps = Fingerprint.objects.all()
    total = len(fps)
    count = 0
    s1 = set([','.join([p.name, p.filename, p.description, p.version])
              for p in fp.plugins.all()])
    for f in fps:
        s2 = set([','.join([p.name, p.filename, p.description, p.version])
                  for p in f.plugins.all()])
        if s1 == s2:
            count = count+1
    return count/total*100
