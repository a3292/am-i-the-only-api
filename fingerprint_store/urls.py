# myapi/urls.py

from django.urls import path

from fingerprint_store.views import get_fingerprint, create_fingerprint

urlpatterns = [
    path('createFingerprint', create_fingerprint),
    path('getFingerprint', get_fingerprint),
]
